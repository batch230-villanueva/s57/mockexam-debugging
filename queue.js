let collection = [];

// Write the queue functions below.
// (1) Make sure to have correct function names

function print() {
    console.log('CONTENTS:')

    for (let i = 0; i < collection.length; i++) {
        console.log(collection[i]);
    }

    console.log('==============');

    return collection;
}

function enqueue(item) {
    collection[collection.length] = item;

    return collection;
}

function dequeue() {
    collection.splice(0,1)

    return collection;
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    if(collection.length) return false;
    return true;
}

// (2) Make sure to include the functions in exportation
module.exports = {
    print, enqueue, dequeue, front, size, isEmpty
};
